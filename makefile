CC=gcc
CFLAGS=-I./include
LIBS=-lpthread

CFLAGS += -g

SRCDIR = ./src/
HDIR = ./include/

SRCS = memorymanager.c

OBJS = memorymanager.o
HOST = $(SRCDIR)Communicator.o
GAST = $(SRCDIR)test.o
OBJECT = $(patsubst %.o,$(SRCDIR)%.o,$(OBJS))

server: $(HOST) $(OBJECT)
	$(CC) -o CommunicatorServer.exe $(HOST) $(OBJECT) $(LDFLAGS) $(LIBS)

client: $(GAST) $(OBJECT)
	$(CC) -o CommunicatorClient.exe $(GAST) $(OBJECT) $(LDFLAGS) $(LIBS)

$(OBJECT) : $(HDIR)datastructs.h

$(SRCDIR)memorymanager.o : $(HDIR)memorymanager.h

$(HOST) : $(HDIR)datastructs.h

$(GAST) : $(HDIR)datastructs.h
