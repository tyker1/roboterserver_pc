#ifndef __DATASTRUCTS_H__
#define __DATASTRUCTS_H__

#include <stdint.h>

#define LINUX
#define BUFFERSIZE 2048
// Size of memory file is set to 4k because its just fit the page size under windows
#define FILESIZE 4096

#ifdef WINDOWS

#define SHM_NAME_RECOGNITION ("Global\\Recognition")
#define SHM_NAME_SYNTHESE ("Global\\Synthese")
#define MUTEX_NAME_RECOGNITION_GLOBAL ("Global\\Recognition\\Mutex\\Global")
#define MUTEX_NAME_RECOGNITION_BUFFER ("Global\\Recognition\\Mutex\\Buffer")
#define MUTEX_NAME_SYNTHESE_GLOBAL ("Global\\Synthese\\Mutex\\Global")
#define MUTEX_NAME_SYNTHESE_BUFFER ("Global\\Synthese\\Mutex\\Buffer")

#endif

#ifdef LINUX

#define SHM_PATH ("./RoboterCommunicator.shm")
#define SEMAPHORE_RECOGNITION_FULL ("RecoginitionFull.key")
#define SEMAPHORE_RECOGNITION_EMPTY ("RecoginitionEmpty.key")
#define SEMAPHORE_SYNTHESE_FULL ("SyntheseFull.key")
#define SEMAPHORE_SYNTHESE_EMPTY ("SyntheseEmpty.key")

#endif

struct TSHMRecognition
{
    int iSegmentSize;
    unsigned char cDataBuffer[BUFFERSIZE];
    int bResultRdy;
    int iAligment;
    uint16_t iResultCode;
};

struct TSHMSynthese
{
    uint16_t iRegcognitionCode;
    int iBytesWrite;
    int iAlignment;
    unsigned char cDataBuffer[BUFFERSIZE];
};

struct TSHMCommunication
{
    struct TSHMRecognition tRecognition;
    struct TSHMSynthese tSynthese;
};

#endif /* __DATASTRUCTS_H__ */
