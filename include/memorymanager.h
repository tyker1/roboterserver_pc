#ifndef MEMORYMANAGER_H
#define MEMORYMANAGER_H

#include <sys/types.h>
void FreeSharedMemory(void * pMap, int iMemSize);
void * CreateSharedMemory(const char * sPath, int bReset, int iBufferSize, int iOffset);





#endif /* MEMORYMANAGER_H */
