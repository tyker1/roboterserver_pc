#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "memorymanager.h"

void * CreateSharedMemory(const char * sPath, int bReset, int iBufferSize, int iOffset)
{
    int flags;
    int fd;
    void * pMemPointer;

    flags = O_CREAT | O_RDWR;

    if (bReset == 1)
    {
        flags |= O_TRUNC;
    }

    //fd = open(sPath, flags, 0666); // RW-Access for all user
    fd = open(sPath, flags);
    if (bReset == 1)
    {
        lseek(fd,iBufferSize - 1, SEEK_SET);
        write(fd,"",1);
    }

    pMemPointer = mmap(NULL, iBufferSize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, iOffset);

    return pMemPointer;
}

void FreeSharedMemory(void * pMap, int iMemSize)
{
    munmap(pMap, iMemSize);
}
