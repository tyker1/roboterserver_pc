#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>
#include <time.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <sys/mman.h>

#include "datastructs.h"
#include "memorymanager.h"

#define LOOPS 5
#define DATASIZE 10

int main(int argc, char *argv[])
{

    struct TSHMCommunication * tSharedMemory;
    struct TSHMRecognition * tSharedRecognition;
    sem_t *full;
    sem_t *empty;
    int i = LOOPS;
    int j;

    sem_unlink(SEMAPHORE_RECOGNITION_EMPTY);
    sem_unlink(SEMAPHORE_RECOGNITION_FULL);

    full = sem_open(SEMAPHORE_RECOGNITION_FULL, O_CREAT, 0644,0);
    if (full == SEM_FAILED)
    {
        printf("Cannot initiallize FULL Semaphore\n");
        exit(1);
    }
    empty = sem_open(SEMAPHORE_RECOGNITION_EMPTY, O_CREAT, 0644,1);
    if (empty == SEM_FAILED)
    {
        printf("Cannot initiallize EMPTY Semaphore\n");
        exit(1);
    }


    tSharedMemory = CreateSharedMemory(SHM_PATH, 1, sizeof(struct TSHMCommunication), 0);

    if (tSharedMemory == MAP_FAILED)
    {
        perror("Error while getting shared memory");
        sem_close(empty);
        sem_close(full);
        sem_unlink(SEMAPHORE_RECOGNITION_EMPTY);
        sem_unlink(SEMAPHORE_RECOGNITION_FULL);
        return -1;
    }

    tSharedRecognition = &(tSharedMemory->tRecognition);

    srand(time(0));

    while (i-- > 0)
    {
        sem_wait(empty);
        tSharedRecognition->iAligment = 8;
        tSharedRecognition->iSegmentSize = DATASIZE;
        printf("*************************\nServer Generated[%d]:\n",i);
        for (j = 0; j < DATASIZE; ++j)
        {
            tSharedRecognition->cDataBuffer[j] = rand() % 255;
            printf("%d ", tSharedRecognition->cDataBuffer[j]);
        }
        printf("\n");
        if (tSharedRecognition->bResultRdy == 1)
        {
            printf("Result = %d\n", tSharedRecognition->iResultCode);
            tSharedRecognition->bResultRdy = 0;
        }
        sem_post(full);
    }


    sem_close(empty);
    sem_close(full);


    FreeSharedMemory(tSharedMemory, sizeof(struct TSHMCommunication));

    sem_unlink(SEMAPHORE_RECOGNITION_EMPTY);
    sem_unlink(SEMAPHORE_RECOGNITION_FULL);
    return 0;
}
