#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>
#include <time.h>
#include <sys/stat.h>
#include <semaphore.h>

#include "datastructs.h"
#include "memorymanager.h"

#define LOOPS 5
#define DATASIZE 10

int main(int argc, char *argv[])
{

    struct TSHMCommunication * tSharedMemory;
    struct TSHMRecognition * tSharedRecognition;
    sem_t *full;
    sem_t *empty;
    int i = LOOPS;
    int j;
    full = sem_open(SEMAPHORE_RECOGNITION_FULL, O_CREAT, 0644,0);
    if (full == SEM_FAILED)
    {
        printf("Cannot initiallize FULL Semaphore\n");
        exit(1);
    }
    empty = sem_open(SEMAPHORE_RECOGNITION_EMPTY, O_CREAT, 0644,1);
    if (empty == SEM_FAILED)
    {
        printf("Cannot initiallize EMPTY Semaphore\n");
        exit(1);
    }


    tSharedMemory = CreateSharedMemory(SHM_PATH, 0, sizeof(struct TSHMCommunication), 0);
    tSharedRecognition = &(tSharedMemory->tRecognition);

    srand(time(0));
    printf("start loop\n");
    while (i-- > 0)
    {
        sem_wait(full);
        printf("********************************\nClient Recieved[%d]:\n",j);
        for (j = 0; j < DATASIZE; ++j)
        {
            printf("%d ", tSharedRecognition->cDataBuffer[j]);
        }
        printf("\n");
        tSharedRecognition->iResultCode = rand() % 65535;
        tSharedRecognition->bResultRdy = 1;
        printf("Client Genrated Code : %d\n", tSharedRecognition->iResultCode);

        sem_post(empty);
    }


    sem_close(empty);
    sem_close(full);

    FreeSharedMemory(tSharedMemory, sizeof(struct TSHMCommunication));

    return 0;
}
